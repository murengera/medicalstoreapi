from django.db import models
import uuid
class Employee(models.Model):

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name=models.CharField(max_length=255)
    joining_date=models.DateField()
    phone=models.CharField(max_length=255)
    address=models.CharField(max_length=255)
    added_on=models.DateTimeField(auto_now_add=True)


class EmployeeSalary(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    employee=models.ForeignKey(Employee,on_delete=models.CASCADE)
    salary_date=models.DateField()
    salary_amount=models.CharField(max_length=255)
    added_on=models.DateField(auto_now_add=True)




class EmployeeBank(models.Model):

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)

    bank_account_no=models.CharField(max_length=255)
    ifsc_no=models.CharField(max_length=255)
    employee=models.ForeignKey(Employee,on_delete=models.CASCADE)
    added_on=models.DateTimeField(Employee,max_length=255)
