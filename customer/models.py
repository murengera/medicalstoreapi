from django.db import models

import  uuid

class Customer(models.Model):
        id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
        name=models.CharField(max_length=255)
        address=models.CharField(max_length=255)
        contact=models.CharField(max_length=255)
        added_on=models.CharField(max_length=255)
