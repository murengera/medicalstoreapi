from django.contrib import admin

from  company.models import  Company,CompanyAccount,CompanyBank
admin.site.register(CompanyBank)
admin.site.register(Company)
admin.site.register(CompanyAccount)

