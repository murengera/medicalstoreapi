from django.db import models
import  uuid
class Company(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name=models.CharField(max_length=255)
    licence_no=models.CharField(max_length=225)
    address=models.CharField(max_length=255)
    contact_no=models.CharField(max_length=255)
    email=models.CharField(max_length=255)
    desription=models.CharField(max_length=255)
    added_on=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return  self.name

class CompanyAccount(models.Model):
    choices = (
        ('Debit', 'Debit'),
        ('Credit', 'Credit'),

    )

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    company=models.ForeignKey(Company,on_delete=models.CASCADE)
    transaction_type = models.CharField(max_length=15, choices=choices, default="Debit")
    transaction_amt=models.CharField(max_length=255)
    transaction_date=models.DateField()
    payment_mode=models.CharField(max_length=255)

class  CompanyBank(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    bank_account_no=models.CharField(max_length=255)
    ifsc_no=models.CharField(max_length=255)
    company=models.ForeignKey(Company,on_delete=models.CASCADE)
    added_on=models.DateTimeField(auto_now_add=True)


