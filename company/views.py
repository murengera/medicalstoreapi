from django.shortcuts import render
from  company.models import *
from rest_framework import viewsets
from company.serializers import CompanySerializer

class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
